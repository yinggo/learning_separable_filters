function [] = save_reconstructed_filter_bank(p,M,count,original_filters)
%  save_reconstructed_filter_bank  Dump to disk the filter bank reconstructed
%                                  by the current approximating filters
%
%  Synopsis:
%     save_reconstructed_filter_bank(p,M,count,original_filters)
%
%  Input:
%     p                = simulation's parameters
%     M                = separable filter bank
%     count            = iteration count
%     original_filters = approximated filter bank
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

fd = fopen(fullfile(p.reconstructed_filters_txt_directory,sprintf('fb_%06d.txt',count/p.steps_no)),'wt');

rows_no = p.filters_size;
cols_no = p.filters_size;
rec_fb = zeros(rows_no*cols_no,p.original_fb_no);
rec_t = zeros(p.filters_no,p.original_fb_no);
sampling_prob = zeros(p.original_fb_no,1);

for i_x = 1:p.original_fb_no
    x = original_filters(:,:,i_x);
    x = x(:);
    
    t = best_optimize_coeffs(p,M,x,1e-7);
    
    rec = M*t;
    rec_fb(:,i_x) = rec;
    rec_t(:,i_x) = t(1:p.filters_no);
    rec = reshape(rec,[p.filters_size,p.filters_size]);
    for r = 1:p.filters_size
        fprintf(fd,'%f ',rec(r,:));
        fprintf(fd,'\n');
    end 
end
fclose(fd);

save(fullfile(p.coeffs_txt_directory,sprintf('coeffs_t_%06d.txt',count/p.steps_no)),'rec_t','-ascii')
nrrdSave(sprintf('%s/fb_%06d.nrrd',p.reconstructed_filters_img_directory,floor(count/p.steps_no)),reshape_result_fb_as_img(p,rec_fb,p.original_fb_no)');

end

