function [] = save_filter_bank(p,M,count)
%  save_filter_bank  Dump to disk the separable filter bank
%
%  Synopsis:
%     save_filter_bank(p,M,count)
%
%  Input:
%     p     = simulation's parameters
%     M     = separable filter bank
%     count = iteration count
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

fd = fopen(fullfile(p.filters_txt_directory,sprintf('fb_%06d.txt',count/p.steps_no)),'wt');

for i_f = 1:p.filters_no
    filter = reshape(M(:,i_f),[p.filters_size,p.filters_size]);
    for r = 1:p.filters_size
        fprintf(fd,'%f ',filter(r,:));
        fprintf(fd,'\n');
    end 
end

fclose(fd);

end

