function [p] = get_config(resume_fb)
%  get_config  Setup file for the approximation algorithm.
%
%  Synopsis:
%     get_config(resume_fb)
%
%  Input:
%     resume_fb = number of the filter bank to resume
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

p.filters_txt_directory = 'filters_txt';
p.filters_img_directory = 'filters_img';
p.reconstructed_filters_txt_directory = 'reconstructed_filters_txt';
p.reconstructed_filters_img_directory = 'reconstructed_filters_img';
p.coeffs_txt_directory = 'coeffs_rec_txt';

if (resume_fb==0)
    [status, message, messageid] = rmdir(p.filters_txt_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.filters_img_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.reconstructed_filters_txt_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.reconstructed_filters_img_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = rmdir(p.coeffs_txt_directory, 's'); %#ok<*NASGU,*ASGLU>
    [status, message, messageid] = mkdir(p.filters_txt_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.filters_img_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.reconstructed_filters_txt_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.reconstructed_filters_img_directory); %#ok<*ASGLU,*NASGU>
    [status, message, messageid] = mkdir(p.coeffs_txt_directory); %#ok<*ASGLU,*NASGU>
end

% Filter bank path
p.original_filters_filename = 'data/learned_121_DRIVE.txt';

p.gradient_steps_no = 10;
% Gradient step for the coefficients
p.gradient_step_size = 1e-1;
% Gradient step for the filters
p.filters_grad_step_size = 7e-2;

% As we are not interested in sparsity, we can keep the l1 penalty to zero (though it can be interesting to explore it!)
p.lambda_l1 = 0;
% Nuclear norm regularization parameter
p.lambda_nuclear = 1e-2;

% Number of desired filters in the separable filter bank
p.filters_no = 25;

% Number of steps performed before dumping to disk
p.steps_no = 250;
% Visualization parameters
p.vSpace = 4;
p.hSpace = 4;
p.pixS = 2;

end

