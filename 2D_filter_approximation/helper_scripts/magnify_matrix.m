function [m_mat] = magnify_matrix(in_mat,factor)
%  magnify_matrix  Magnify an input matrix for visualization purposes
%
%  Synopsis:
%     [m_mat] = magnify_matrix(in_mat,factor)
%
%  Input:
%     in_mat = input matrix
%     factor = magnification factor
%  Output:
%     m_mat = magnified matrix   
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

m_mat = zeros(size(in_mat,1)*factor,size(in_mat,2)*factor);

for r = 1:size(in_mat,1)
    for c = 1:size(in_mat,2)
        m_mat((r-1)*factor+1:r*factor,(c-1)*factor+1:c*factor) = in_mat(r,c);
    end
end 

end

