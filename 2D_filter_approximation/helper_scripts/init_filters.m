function [M] = init_filters(p,resume_fb_number)
%  init_filters  Initialize the approximating filter bank
%
%  Synopsis:
%     [M] = init_filters(p,resume_fb_number)
%
%  Input:
%     p         = simulation's parameters
%     resume_fb = number of the filter bank to resume (if !=0)
%  Output:
%     M = created filter bank (each column is a filter)
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

if (resume_fb_number==0)
    M = randn(p.filters_size^2);
    M(:,1) = ones(p.filters_size^2,1);
else
    M = zeros(p.filters_size^2);
    fb = load(fullfile(p.filters_txt_directory,sprintf('fb_%06d.txt',resume_fb_number)));
    filter_size = size(fb,2);
    for i_filter = 1:p.filters_no
    	filt = fb((i_filter-1)*filter_size+1:i_filter*filter_size,:);
	M(:,i_filter) = filt(:);
    end
end

M(:,p.filters_no+1:end) = 0;
M = normalize_fb(p,M);

end

