function [M] = apply_componentwise_NN_proximal_operator(p,M)
%  apply_componentwise_NN_proximal_operator  Apply the proximal operator for
%                                            the nuclear norm to each filter
%
%  Synopsis:
%     [M] = apply_componentwise_NN_proximal_operator(p,M)
%
%  Input:
%     p = simulation's parameters
%     M = separable filter bank
%  Output:
%     M = separable filter bank after the proximal operator has been applied
%
%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: April 2013
%  last revision: 10 April 2013

for i_f = 1:p.filters_no
    [U,S,V] = svd(reshape(M(:,i_f),[p.filters_size,p.filters_size]));

    % ST step
    s = diag(S);
    s(abs(s)<p.lambda_nuclear) = 0;
    s(s>=p.lambda_nuclear) = s(s>=p.lambda_nuclear)-p.lambda_nuclear;
    s(s<=-p.lambda_nuclear) = s(s<=-p.lambda_nuclear)+p.lambda_nuclear;

    th_S = diag(s);
    
    recomp = U*th_S*V';
    M(:,i_f) = recomp(:);
end

end
