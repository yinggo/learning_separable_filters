function [img] = convert_img_visualization(in_mat)

img = zeros(size(in_mat));

min_val = min(in_mat(:));
max_val = max(in_mat(:));
extr = max(abs(min_val),abs(max_val));

if(max_val-min_val<1e-5 || extr<1e-5)
    img(:,:) = 127.5;
else
    img = in_mat./extr;
    img = img+1;
    img = img*127.5;
end

end

