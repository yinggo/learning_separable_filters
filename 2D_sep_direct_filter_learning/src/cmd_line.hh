/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef CMD_LINE_HH
#define CMD_LINE_HH

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <boost/config.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include "exceptions.hh"
#include "utils.hh"

#define VERSION_NUMBER    "1.0"

namespace po = boost::program_options;
namespace fs = boost::filesystem;

class CmdLine {
public:
  CmdLine(int argc,char** argv);
  ~CmdLine() {};

  bool resume() const { return(m_resume_flag); };
  bool clear() const { return(m_clear_flag); };
  bool clearall() const { return(m_clearall_flag); };
  std::string get_resume_sim_filename() const {	return(m_resume_sim_filename); }
  unsigned int get_resumed_it_number() const {
	if (!m_resume_flag)
	  throw ResumeException("No resume flag specified, impossible to retrieve a previous iteration number");
	return(m_resumed_it_number);
  }
  std::string get_resumed_fb_filename() const {
	if (!m_resume_flag)
	  throw ResumeException("No resume flag specified, impossible to retrieve the filter bank from the previous simulation");
	return(m_resumed_fb_filename);
  }
  std::string get_config_filename() const {
	return(m_config_filename);
  }

private:
  bool m_resume_flag;
  bool m_clear_flag;
  bool m_clearall_flag;
  std::string m_config_filename;
  std::string m_resume_sim_filename;
  unsigned int m_resumed_it_number;
  std::string m_resumed_fb_filename;
};

#endif // CMD_LINE_HH
