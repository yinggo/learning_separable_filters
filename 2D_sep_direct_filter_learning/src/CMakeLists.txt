###############################################################################
#   Copyright (C) 2012                                                        #
#   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]          #
#   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]                             #
#                                                                             #
#    This program is free software: you can redistribute it and/or modify     #
#    it under the terms of the GNU General Public License as published by     #
#    the Free Software Foundation, either version 3 of the License, or        #
#    (at your option) any later version.                                      #
#                                                                             #
#    This program is distributed in the hope that it will be useful,          #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of           #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
#    GNU General Public License for more details.                             #
#                                                                             #
#    You should have received a copy of the GNU General Public License        #
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.    #
###############################################################################

FILE(GLOB dir_source *.cc 2D/*.cc)
FILE(GLOB dir_header *.hh 2D/*.hh)

add_executable(${PROJECT_NAME} ${dir_source} ${dir_header})
target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS} ${Boost_PROGRAM_OPTIONS_LIBRARY} ${Boost_FILESYSTEM_LIBRARY} ${Boost_SYSTEM_LIBRARY})
