/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#ifndef DATASET_2D_SS_HH
#define DATASET_2D_SS_HH

#include <iostream>
#include <fstream>
#include <utility>
#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>
#include "opencv2/opencv.hpp"

#include "../dataset.hh"
#include "../parameters.hh"
#include "../exceptions.hh"
#include "../utils.hh"
#include "../filter_bank.hh"

// Maximum number of attempts performed when randomly selecting row/col number
// for a given image (prevents from being stuck on an "infeasible" image)
#define MAX_ATTEMPTS_IMG      100

namespace fs = boost::filesystem;

class Dataset_2D_SS : public Dataset {
public:
  Dataset_2D_SS(Parameters& config,boost::shared_ptr< Filter_bank >& filters,cv::RNG& rng);
  virtual ~Dataset_2D_SS() { };

  virtual cv::Mat get_sample() const;

  void get_sample_dimensions(struct sample_dimensions& dims) const {
	dims.dims_no = 2;
	dims.user_sizes.clear();
	dims.extended_sizes.clear();
	dims.user_sizes.push_back(m_sample_size);
	dims.user_sizes.push_back(m_sample_size);
	dims.extended_sizes.push_back(m_enlarged_sample_size);
	dims.extended_sizes.push_back(m_enlarged_sample_size);
  }

protected:
  virtual void parse_dataset_config(const std::string& dataset_config_filename);
  virtual void load_data(std::vector< cv::Mat >& dst_vector,const std::string& data_list_filename,const bool binarize_flag=false);
  void create_empty_masks();

  unsigned int m_enlarged_sample_size;
  unsigned int m_sample_size;
  float m_sample_min_stddev;
  bool m_sample_based_normalization;

  unsigned int m_images_no;

  bool m_use_masks;
  bool m_binarize_masks;

  std::string m_images_list_filename;
  std::string m_masks_list_filename;

  std::vector< cv::Mat > m_images;
  std::vector< cv::Mat > m_masks;
};

#endif // DATASET_2D_SS_HH
