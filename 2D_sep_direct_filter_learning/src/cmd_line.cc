/******************************************************************************
   Copyright (C) 2012
   Roberto Rigamonti [ roberto <dot> rigamonti <at> epfl <dot> ch ]
   CVLab EPFL [ http://cvlab.epfl.ch/~rigamont ]

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
******************************************************************************/

#include "cmd_line.hh"

CmdLine::CmdLine(int argc,char** argv) : m_resume_flag(false),m_resumed_it_number(0),m_resumed_fb_filename("") {
  po::options_description cmd_line_descr("Command line parameters");
  // Command line option list
  cmd_line_descr.add_options()
	("cf", po::value< std::string >(&(m_config_filename)),
	 "General configuration filename")
    ("clear",
     "Removes the results of a previous simulation")
    ("clearall",
     "Removes the WHOLE previous simulation")
    ("help",
     "Display this message")
	("resume", po::value< std::string >(&(m_resume_sim_filename)),
	 "Resume a given simulation")
    ("version",
     "Return the version number");

  // Parse command line
  po::variables_map cmd_line_vm;
  po::store(po::parse_command_line(argc,argv,cmd_line_descr),cmd_line_vm);
  po::notify(cmd_line_vm);

  if (cmd_line_vm.count("help")) {
	std::cerr << cmd_line_descr << std::endl;
	exit(0);
  }

  if (cmd_line_vm.count("version")) {
	std::cerr << "Version number : " << VERSION_NUMBER << std::endl;
	exit(0);
  }

  if (!cmd_line_vm.count("cf")) {
	throw ConfigParseException("The general configuration filename has not been specified");
  }

  if (cmd_line_vm.count("resume") && (cmd_line_vm.count("clear") || cmd_line_vm.count("clearall"))) {
	throw ConfigParseException("Requested both resume of the previous simulation and removal of previous simulation's data");
  }

  m_clearall_flag = false;
  m_clear_flag = false;
  if (cmd_line_vm.count("clearall")) {
	m_clearall_flag = true;
  }
  else {
	if (cmd_line_vm.count("clear")) {
	  m_clear_flag = true;
	}
  }

  if (cmd_line_vm.count("resume")) {
	m_resume_flag = true;
	std::cerr << "  Resuming a previous simulation" << std::endl;
	if (Utils::file_exists(m_resume_sim_filename))
	  std::cerr << "    + Previous simulation file present" << std::endl;
	else {
	  std::string err_msg("Impossible to locate the previous simulation file "+m_resume_sim_filename);
	  throw ResumeException(err_msg.c_str());
	}

	po::options_description simulation_file_descr("Previous simulation data");
	simulation_file_descr.add_options()
	  ("filter_bank_path", po::value< std::string >(&(m_resumed_fb_filename)),
	   "Path of the last saved filter bank")
	  ("iteration_number", po::value< unsigned int >(&(m_resumed_it_number)),
	   "Iteration number from which the simulation will start");

	// Parse config file
	std::ifstream ifs(m_resume_sim_filename.c_str());
	if (!ifs.is_open()) {
	  std::string err_msg("Unable to read the previous simulation file "+m_resume_sim_filename);
	  throw ResumeException(err_msg.c_str());
	}
	po::variables_map sim_file_vm;
	po::store(po::parse_config_file(ifs,simulation_file_descr),sim_file_vm);
	po::notify(sim_file_vm);
	ifs.close();
  }
}
