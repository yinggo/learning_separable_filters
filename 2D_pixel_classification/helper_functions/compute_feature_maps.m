function [fm_count] = compute_feature_maps(p,img,mask,fm_output_dir,fm_file_format,fm_count)
%  compute_feature_maps  Extracts the feature maps from a given image using the
%                        specified filter bank
%
%  Synopsis:
%     [fm_count] = compute_feature_maps(p,img,mask,fm_output_dir,fm_file_format,fm_count)
%
%  Input:
%     p              = structure containing system's configuration and paths
%     img            = source image for feature maps' computation
%     mask           = mask for the input image
%     fm_output_dir  = directory where the feature maps are stored
%     fm_file_format = file format for the saved feature maps
%     fm_count       = feature maps' count (used to generate the progressive
%                      filename)
%  Output:
%     fm_count = incremental counter for the feature maps

%  author: Roberto Rigamonti, CVLab EPFL
%  e-mail: roberto <dot> rigamonti <at> epfl <dot> ch
%  web: http://cvlab.epfl.ch/~rigamont
%  date: June 2012
%  last revision: 12 April 2013

fb = p.filter_bank.fb;
filters_no = p.filter_bank.no;
if (p.separable_filters)
    original_filters_no = size(p.fb_coeffs,2);
    tmp_fm = cell(filters_no,1);
else
    original_filters_no = filters_no;
end
feature_maps = cell(original_filters_no,1);

% Normalize the image before filtering
img = normalize_in_mask(img,mask);

for i_fm = 1:filters_no
    % Perform the convolution
    if (p.separable_filters)
        fm_a = imfilter(img,fb{i_fm,1},'same','corr');
        tmp_fm{i_fm} = imfilter(fm_a,fb{i_fm,2}','same','corr');
    else
        feature_maps{i_fm} = imfilter(img,fb{i_fm},'same','corr');
    end
end

% ** NON-OPTIMIZED ** reconstruction of the feature maps
if (p.separable_filters)
    rows_no = size(tmp_fm{1},1);
    cols_no = size(tmp_fm{1},2);
    for i_orig_filter = 1:original_filters_no
        feature_maps{i_orig_filter} = zeros(rows_no,cols_no);
        for i_filter = 1:filters_no
            feature_maps{i_orig_filter} = feature_maps{i_orig_filter}+p.fb_coeffs(i_filter,i_orig_filter)*tmp_fm{i_filter};
        end
    end
end

for i_fm = 1:original_filters_no
    % Re-normalize the feature map
    feature_map = normalize_in_mask(feature_maps{i_fm},mask);
        
    % Dump response to file
    feature_map(mask==0) = 0;
    nrrdSave(sprintf(fm_file_format,fm_output_dir,fm_count),feature_map');
    fm_count = fm_count+1;
end

end
